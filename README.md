# ember-cli-foundation-modal

[![Build Status][]](https://travis-ci.org/toranb/ember-cli-foundation-modal)
[![NPM Downloads][]](https://www.npmjs.org/package/ember-cli-foundation-modal)

## Description

[ember-cli][] addon that sets the foundation reveal root_element to a value that you customize or a reasonable default.

## Installation

```
npm install ember-cli-foundation-modal --save-dev
```

## To use this modal in your app

bower install zurb foundation 5.5

```js
bower install foundation --save
```

## Running Tests

    npm install
    bower install
    ember test

## License

Copyright © 2015 Toran Billups http://toranbillups.com

Licensed under the MIT License

[NPM Downloads]: https://img.shields.io/npm/dm/ember-cli-foundation-modal.svg
[Build Status]: https://travis-ci.org/toranb/ember-cli-foundation-modal.svg?branch=master
[ember-cli]: http://www.ember-cli.com/
[ember.js]: http://emberjs.com/
